<DOCTYPE html>
<html>
    <head>
        <title>Tic Tac Toe</title>
        <meta name="application-details" content="Tic Tac Toe Application" data-human-unit="{{ $human_unit }}" data-bot-unit="{{ $bot_unit }}" />
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
        <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="{{ asset('js/app.min.js') }}"></script>
        <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="{{ asset('css/app.min.css') }}" rel="stylesheet">
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-12 center-block full-height">
                    <div class="full-height">
                        <div id="match_status" class="row text-center pull-left">
                            <div id="result" class="pull-left"></div>
                            <div id="play_again" class="pull-left">
                                Play again?
                                <input type="button" onclick="restartGame()" value="Yes" />
                                <input type="button" onclick="finishGame()" value="No" />
                            </div>
                        </div>
                        <div id="results" class="pull-right">
                            Ninja Bot <strong id="player_X">0</strong> vs <strong id="player_O">0</strong> <span id="label_name"></span>
                        </div>
                        <table id="board" class="table table-bordered full-height text-center">
                            <tr>
                                <td id="coord_0-0" data-coord-x="0" data-coord-y="0"></td>
                                <td id="coord_0-1" data-coord-x="0" data-coord-y="1"></td>
                                <td id="coord_0-2" data-coord-x="0" data-coord-y="2"></td>
                            </tr>
                            <tr>
                                <td id="coord_1-0" data-coord-x="1" data-coord-y="0"></td>
                                <td id="coord_1-1" data-coord-x="1" data-coord-y="1"></td>
                                <td id="coord_1-2" data-coord-x="1" data-coord-y="2"></td>
                            </tr>
                            <tr>
                                <td id="coord_2-0" data-coord-x="2" data-coord-y="0"></td>
                                <td id="coord_2-1" data-coord-x="2" data-coord-y="1"></td>
                                <td id="coord_2-2" data-coord-x="2" data-coord-y="2"></td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="welcome" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <form class="form-horizontal" id="form" action="#">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title">Welcome to Tic Tac Toe! Can you defeat the ninja bot? Let's see!</h4>
                            </div>
                            <div class="modal-body">
                                <input type="text" class="form-control" id="player_name" name="name" required="required" placeholder="Your name..." value="" />
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-primary" id="btn_lote">Play NOW!</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
</html>