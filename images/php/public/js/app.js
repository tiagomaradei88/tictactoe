var board = [['', '', ''], ['', '', ''], ['', '', '']],
    myEvent = window.attachEvent || window.addEventListener,
    chkevent = window.attachEvent ? 'onbeforeunload' : 'beforeunload',
    details = $('meta[name="application-details"]'),
    meta = $('meta[name="csrf-token"]').attr('content'),
    human_unit = details.attr('data-human-unit'),
    bot_unit = details.attr('data-bot-unit'),
    human_icon = human_unit == 'O' ? 'fa-circle-o' : 'fa-times',
    bot_icon = human_unit == 'O' ? 'fa-times' : 'fa-circle-o',
    starting_match = human_unit,
    human_wins = 0;
    bot_wins = 0,
    tied = 0
    match_finished = false,
    player_name = '';

myEvent(chkevent, function(e) {
    var confirmationMessage = ' ';
    (e || window.event).returnValue = confirmationMessage;
    return confirmationMessage;
});

$(document).ready(function(){

    $('#board td').on('click', function(){

        let item = $(this), x = item.attr('data-coord-x'), y = item.attr('data-coord-y');

        if(board[x][y] || match_finished) {
            return false;
        }

        item.html(`<i class="fa ${human_icon}" aria-hidden="true"></i>`);
        board[x][y] = human_unit;

        checkMatchStatus();
        makeMove();
    });

    $("#form").submit(function(e){

        e.preventDefault();

        let name = $('#player_name').val();

        if(name) {

            player_name = name;

            $('#label_name').html(player_name);
            $('#welcome').modal('toggle');
            $('#results').show();
        }
    });

    $('#welcome').modal('toggle');
});

function makeMove() {

    if(match_finished) {
        return false;
    }

    $.ajax({
        url: "/makemove",
        type: "post",
        headers: {
            'X-CSRF-TOKEN': meta
        },
        data: {
            "boardState": board
        },
        async: false,
        success: function(response) {

            if(response) {

                var x = response[0], y = response[1], unit = response[2];
                board[x][y] = unit;

                $(`#coord_${x}-${y}`).html(`<i class="fa ${bot_icon}" aria-hidden="true"></i>`);
            }

            checkMatchStatus();
        }
    });
}

function checkMatchStatus() {
    $.ajax({
        url: "/checkstatus",
        type: "post",
        headers: {
            'X-CSRF-TOKEN': meta
        },
        data: {
            "boardState": board
        },
        async: false,
        success: function(response) {

            if(response.finished) {

                let result = $('#result'), label_name = player_name;
                match_finished = true;

                if(response.playerWinner) {

                    if(response.playerWinner == human_unit) {
                        human_wins++;
                    } else {
                        label_name = 'Ninja Bot';
                        bot_wins++;
                    }

                    result.html(`${label_name} is the winner! :)`);
                } else {
                    result.html('Tied match! :(');
                    tied++;
                }

                $('#match_status').show();
                $('#board').addClass('table_disabled');
                $(`#player_${human_unit}`).html(human_wins);
                $(`#player_${bot_unit}`).html(bot_wins);
            }
        }
    });
}

function restartGame() {

    board = [['', '', ''], ['', '', ''], ['', '', '']];
    match_finished = false;

    let table = $('#board');
    table.removeClass('table_disabled');
    table.find('td').each(function() { $(this).html('') });

    $('#match_status').hide();

    if(starting_match == human_unit) {

        let coord_x = Math.floor((Math.random() * 3) + 1) - 1, coord_y = Math.floor((Math.random() * 3) + 1) - 1;

        board[coord_x][coord_y] = bot_unit;
        starting_match = bot_unit;

        $(`#coord_${coord_x}-${coord_y}`).html(`<i class="fa ${bot_icon}" aria-hidden="true"></i>`);
    } else {
        starting_match = human_unit;
    }
}

function finishGame() {
    $('#play_again').hide();
    $('#result').html(`Thanks for playing ${player_name}! :)`);
}