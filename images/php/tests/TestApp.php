<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class TestApp extends TestCase
{
    /**
     * Test testIndexPage
     *
     * @return void
     */
    public function testIndexPage()
    {
        $this->visit('/')->see('Tic Tac Toe');
    }

    /**
     * Test testIsMatchFinished
     *
     * @return void
     */
    public function testIsMatchFinished()
    {
        $board = [['X','X','O'], ['O','O','X'], ['X','O','X']];
        $data = $this->json('POST', '/checkstatus', ['boardState' => $board])->response->getContent();
        $response = (array) json_decode($data);

        $this->assertTrue($response['finished'] == true);
    }

    /**
     * Test testIsMatchNotFinished
     *
     * @return void
     */
    public function testIsMatchNotFinished()
    {
        $board = [['X','X','O'], ['O','','X'], ['X','O','X']];
        $data = $this->json('POST', '/checkstatus', ['boardState' => $board])->response->getContent();
        $response = (array) json_decode($data);

        $this->assertTrue($response['finished'] != true);
    }

    /**
     * Test testIsMatchTied
     *
     * @return void
     */
    public function testIsMatchTied()
    {
        $board = [['X','X','O'], ['O','O','X'], ['X','O','X']];
        $data = $this->json('POST', '/checkstatus', ['boardState' => $board])->response->getContent();
        $response = (array) json_decode($data);

        $this->assertTrue($response['playerWinner'] != 'X' && $response['playerWinner'] != 'O');
    }

    /**
     * Test testIsPlayerXWinner
     *
     * @return void
     */
    public function testIsPlayerXWinner()
    {
        $board = [['X','X','O'], ['O','X','X'], ['X','O','X']];
        $data = $this->json('POST', '/checkstatus', ['boardState' => $board])->response->getContent();
        $response = (array) json_decode($data);

        $this->assertTrue($response['playerWinner'] == 'X');
    }

    /**
     * Test testIsPlayerOWinner
     *
     * @return void
     */
    public function testIsPlayerOWinner()
    {
        $board = [['X','O','O'], ['X','O','X'], ['O','O','X']];
        $data = $this->json('POST', '/checkstatus', ['boardState' => $board])->response->getContent();
        $response = (array) json_decode($data);

        $this->assertTrue($response['playerWinner'] == 'O');
    }

    /**
     * Test testBotCanMakeAMove
     *
     * @return void
     */
    public function testBotCanMakeAMove()
    {
        $board = [['O','',''], ['','',''], ['','','']];
        $data = $this->json('POST', '/makemove', ['boardState' => $board])->response->getContent();
        $response = (array) json_decode($data);

        $this->assertTrue(
            count($response) == 3 &&
            is_integer($response[0]) &&
            is_integer($response[1]) && $response[2] == 'X'
        );
    }

    /**
     * Test testBotCanMakeASmartSafeMove
     *
     * @return void
     */
    public function testBotCanMakeASmartSafeMove()
    {
        $board = [['O','',''], ['O','X',''], ['','','']];
        $data = $this->json('POST', '/makemove', ['boardState' => $board])->response->getContent();
        $response = (array) json_decode($data);

        $this->assertTrue(
            count($response) == 3 &&
            $response[0] == 2 &&
            $response[1] == 0 &&
            $response[2] == 'X'
        );
    }

    /**
     * Test testBotCanMakeAMoveToWin
     *
     * @return void
     */
    public function testBotCanMakeAMoveToWin()
    {
        $board = [['X','O',''], ['O','X',''], ['O','','']];
        $data = $this->json('POST', '/makemove', ['boardState' => $board])->response->getContent();
        $response = (array) json_decode($data);

        $this->assertTrue(
            count($response) == 3 &&
            $response[0] == 2 &&
            $response[1] == 2 &&
            $response[2] == 'X'
        );
    }
}
