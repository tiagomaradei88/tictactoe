<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Interfaces\MoveInterface;

class TicTacToe extends Controller implements MoveInterface
{
    /**
     * @var array $winner_draws
    */
	protected static $winner_draws = [
		['0-0', '0-1', '0-2'],
		['1-0', '1-1', '1-2'],
		['2-0', '2-1', '2-2'],
		['0-0', '1-0', '2-0'],
		['0-1', '1-1', '2-1'],
		['0-2', '1-2', '2-2'],
		['0-0', '1-1', '2-2'],
		['0-2', '1-1', '2-0']
	];

    /**
     * @var array $draws_to_win
    */
	protected static $draws_to_win = [
		'0-0' => [['0-0','1-0','2-0'],['0-0','0-1','0-1'],['0-0','1-1','2-2']],
		'0-1' => [['0-1','0-0','0-2'],['0-1','1-1','2-1']],
		'0-2' => [['0-2','0-1','0-0'],['0-2','1-2','2-2'],['0-2','1-1','2-0']],
		'1-0' => [['1-0','1-1','1-2'],['1-0','2-0','0-0']],
		'1-1' => [['1-1','1-0','1-2'],['1-1','0-1','2-1'],['1-1','0-0','2-2'],['1-1','2-0','0-2']],
		'1-2' => [['1-2','0-2','2-2'],['1-2','1-1','1-0']],
		'2-0' => [['2-0','2-1','2-2'],['2-0','1-0','0-0'],['2-0','1-1','0-2']],
		'2-1' => [['2-1','2-0','2-2'],['2-1','1-1','0-1']],
		'2-2' => [['2-2','2-1','2-0'],['2-2','1-2','0-2'],['2-2','1-1','0-0']]
	];

    /**
     * @var string $human_unit
    */
	protected static $human_unit = 'O';

    /**
     * @var string $bot_unit
    */
	protected static $bot_unit = 'X';

    /**
     * Show the board page
     *
     * @return void
    */
    public function show()
    {
    	return view('TicTacToe', ['human_unit' => static::$human_unit, 'bot_unit' => static::$bot_unit]);
    }

    /**
     * Check the match status after each move
     *
     * @param Illuminate\Http\Request $request
     * @return string
    */
    public function checkMatchStatus(Request $request)
    {
    	$boardState = $request->input('boardState');
    	$status = [];

		if($this->isPlayerWinner($boardState, static::$bot_unit)) {
			$status = ['finished' => true, 'playerWinner' => static::$bot_unit];
		} elseif($this->isPlayerWinner($boardState, static::$human_unit)) {
			$status = ['finished' => true, 'playerWinner' => static::$human_unit];
		} else {
			if(static::isMatchFinished($boardState)) {
				$status = ['finished' => true, 'playerWinner' => false];
			} else {
				$status = ['finished' => false, 'playerWinner' => false];
			}
		}

    	return response()->json($status);
    }

    /**
     * Update the board state with bot moves
     *
     * @param Illuminate\Http\Request $request
     * @return string
    */
    public function updateBoardState(Request $request)
    {
    	$boardState = $request->input('boardState');

    	if(!static::isMatchFinished($boardState)) {
    		$next_move = $this->makeMove($boardState, static::$bot_unit);
    		return response()->json($next_move);
    	}
    }

    /**
     * Make a smart move from the bot
     *
     * @param array $boardState
     * @param string $playerUnit
     * @return array
    */
    public function makeMove(array $boardState, $playerUnit = 'X')
    {
    	$best_move = [];
    	$next_move = [];
    	$human_moves = [];
    	$bot_moves = [];

    	for($i = 0; $i <= 2; $i++) {
    		for($j = 0; $j <= 2; $j++) {
    			if($boardState[$i][$j] == static::$human_unit) {
    				$human_moves[] = sprintf('%d-%d', $i, $j);
    			} elseif($boardState[$i][$j] == $playerUnit) {
    				$bot_moves[] = sprintf('%d-%d', $i, $j);
    			}
    		}
    	}

	   	if($human_moves) {

    		for($i = 0, $j = count($human_moves); $i < $j; $i++) {

    			$possibilites = static::$draws_to_win[$human_moves[$i]];

    			for($k = 0, $l = count($possibilites); $k < $l; $k++) {

    				list($x1, $y1) = explode('-', $possibilites[$k][0]);
    				list($x2, $y2) = explode('-', $possibilites[$k][1]);
    				list($x3, $y3) = explode('-', $possibilites[$k][2]);

    				if($boardState[$x1][$y1] == static::$human_unit && $boardState[$x2][$y2] == '' && $boardState[$x3][$y3] == '') {
    					if($boardState[1][1] == '' && (($x1 == 0 && $y1 == 0) || ($x1 == 2 && $y1 == 0) || ($x1 == 2 && $y1 == 2) || ($x1 == 0 && $y1 == 2))) {
    						$best_move[2][] = ['cx' => 1, 'cy' => 1];
    					} else {
    						$best_move[1][] = ['cx' => $x2, 'cy' => $y2];
    					}
    				} elseif($boardState[$x1][$y1] == '' && $boardState[$x2][$y2] == static::$human_unit && $boardState[$x3][$y3] == '') {
    					$best_move[1][] = ['cx' => $x1, 'cy' => $y1];
    				} elseif($boardState[$x1][$y1] == '' && $boardState[$x2][$y2] == '' && $boardState[$x3][$y3] == static::$human_unit) {
    					$best_move[1][] = ['cx' => $x2, 'cy' => $y2];
    				} elseif($boardState[$x1][$y1] == static::$human_unit && $boardState[$x2][$y2] == '' && $boardState[$x3][$y3] == static::$human_unit) {
    					$best_move[3][] = ['cx' => $x2, 'cy' => $y2];
    				} elseif($boardState[$x1][$y1] == static::$human_unit && $boardState[$x2][$y2] == static::$human_unit && $boardState[$x3][$y3] == '') {
    					$best_move[4][] = ['cx' => $x3, 'cy' => $y3];
    				} elseif($boardState[$x1][$y1] == static::$human_unit && $boardState[$x2][$y2] == '' && $boardState[$x3][$y3] == static::$human_unit) {
    					$best_move[5][] = ['cx' => $x2, 'cy' => $y2];
    				} elseif($boardState[$x1][$y1] == '' && $boardState[$x2][$y2] == static::$human_unit && $boardState[$x3][$y3] == static::$human_unit) {
    					$best_move[6][] = ['cx' => $x1, 'cy' => $y1];
    				}
    			}
    		}
		}

		if($bot_moves) {

    		for($i = 0, $j = count($bot_moves); $i < $j; $i++) {

    			$possibilites = static::$draws_to_win[$bot_moves[$i]];

    			for($k = 0, $l = count($possibilites); $k < $l; $k++) {

    				list($x1, $y1) = explode('-', $possibilites[$k][0]);
    				list($x2, $y2) = explode('-', $possibilites[$k][1]);
    				list($x3, $y3) = explode('-', $possibilites[$k][2]);

    				if($boardState[$x1][$y1] == $playerUnit && $boardState[$x2][$y2] == '' && $boardState[$x3][$y3] == $playerUnit) {
    					$best_move[7][] = ['cx' => $x2, 'cy' => $y2];
    				} elseif($boardState[$x1][$y1] == $playerUnit && $boardState[$x2][$y2] == $playerUnit && $boardState[$x3][$y3] == '') {
    					$best_move[8][] = ['cx' => $x3, 'cy' => $y3];
    				} elseif($boardState[$x1][$y1] == $playerUnit && $boardState[$x2][$y2] == '' && $boardState[$x3][$y3] == $playerUnit) {
    					$best_move[9][] = ['cx' => $x2, 'cy' => $y2];
    				} elseif($boardState[$x1][$y1] == '' && $boardState[$x2][$y2] == $playerUnit && $boardState[$x3][$y3] == $playerUnit) {
    					$best_move[10][] = ['cx' => $x1, 'cy' => $y1];
    				}
    			}
    		}
    	}

    	krsort($best_move);

		foreach ($best_move as $score => $moves) {

			$count = count($moves);

			if($count > 1) {
				$sorted_move = rand(0, $count -1);
				$next_move = [$moves[$sorted_move]['cx'], $moves[$sorted_move]['cy'], $playerUnit];
			} else {
				$next_move = [$moves[0]['cx'], $moves[0]['cy'], $playerUnit];
			}

			break;
		}

        if(!$next_move && !static::isMatchFinished($boardState)) {
            list($x, $y) = static::getEmptySpot($boardState);
            $next_move = [$x, $y, $playerUnit];
        }

    	return $next_move;
    }

    /**
     * Check if the match has a winner player
     *
     * @param array $boardState
     * @param string $playerUnit
     * @return bool
    */
    private function isPlayerWinner(array $boardState, $playerUnit) {

    	for($i = 0, $j = count(static::$winner_draws); $i < $j; $i++) {

    		list($x1, $y1) = explode('-', static::$winner_draws[$i][0]);
			list($x2, $y2) = explode('-', static::$winner_draws[$i][1]);
			list($x3, $y3) = explode('-', static::$winner_draws[$i][2]);

			if($boardState[$x1][$y1] == $playerUnit && $boardState[$x2][$y2] == $playerUnit && $boardState[$x3][$y3] == $playerUnit) {
				return true;
			}
    	}

    	return false;
    }

    /**
     * Check if the match is finished
     *
     * @param array $boardState
     * @return bool
    */
    private static function isMatchFinished(array $boardState) {

    	for($i = 0; $i <= 2; $i++) {
    		for($j = 0; $j <= 2; $j++) {
    			if($boardState[$i][$j] == '') {
    				return false;
    			}
    		}
    	}

    	return true;
    }

    /**
     * Check empty spot on the board
     *
     * @param array $boardState
     * @return array
    */
    private static function getEmptySpot(array $boardState) {
        for($i = 0; $i <= 2; $i++) {
            for($j = 0; $j <= 2; $j++) {
                if($boardState[$i][$j] == '') {
                    return [$i, $j];
                }
            }
        }
    }
}