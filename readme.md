# The Tic Tac Toe Game! =)

Are you a good player in Tic Tac Toe? Are you sure? So... I challenge you to defeat the ninja bot!
Install this very funny game and let's see! =)

First, you need to install [Docker CE](https://docs.docker.com/engine/installation/) and [Docker Compose](https://docs.docker.com/compose/install/#install-compose).
Now you just need to follow these few steps below before you can have some fun:

Downloading the game:
```sh
$ git clone https://tiagomaradei88@bitbucket.org/tiagomaradei88/tictactoe.git
```

Starting container:
```sh
$ cd tictactoe
$ sudo docker-compose up --build -d
```

Updating libraries:
```sh
$ sudo docker exec -i test_php /bin/bash -c "composer update"
```

Directory permitions:
```sh
$ sudo docker exec -i test_php /bin/bash -c "chown www-data:www-data /var/www/html/bootstrap/cache -R"
$ sudo docker exec -i test_php /bin/bash -c "chown www-data:www-data /var/www/html/storage -R"
```

Running the game on the browser:
```
http://localhost:8081/
```

Running tests:
```sh
$ sudo docker exec -i test_php /bin/bash -c "vendor/bin/phpunit tests/TestApp.php --testdox"
```